package com.qa.restapi;

//import com.fasterxml.jackson.databind.JsonNode;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;
import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.jsonClasses.GBP;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.qa.client.RestClient.*;
//import io.restassured.RestAssured;
//import io.restassured.http.Method;
//import io.restassured.response.Response;
//import io.restassured.specification.RequestSpecification;

public class RestTest extends TestBase{
    String URI;
    TestBase testBase;

    @BeforeMethod
    public void setup()
    {
        testBase = new TestBase();
        String serviceUrl = property.getProperty("URL");
        String path = property.getProperty("serviceURL");
        String URI = serviceUrl+path;
        System.out.println(URI);
    }
    @Test(priority = 1,enabled = false)
    public void TestResponseAsString()
    {
        //RestClient restClient = new RestClient();
        Response response = getResponseBasedOnQueryParams("https://api.fixer.io/latest","GBP");
        String responseBody = response.getBody().asString();
        System.out.println(responseBody);
        Assert.assertEquals(responseBody,responseBody);
    }

    @Test(priority = 4,enabled = false)
    public void TestResponseForPresenceOfNode()
    {
        Response response = getResponseBasedOnQueryParams("https://api.fixer.io/latest","GBP");
        Assert.assertTrue(checkForPresenseOfNode(response.getBody().asString(),"EUR"),"Node not found in Response");
    }

    @Test(priority = 5)
    public void TestCountOfNode()
    {
        Response response = getResponseBasedOnQueryParams("https://api.fixer.io/latest","GBP");
        JSONObject jsonObj = new JSONObject(response.getBody().asString());
        int count = getCountOfNode(jsonObj,"first_name");
//        int count = getCountOfNode(jsonObj);  // to get the count of all the nodes
        System.out.println(count);
        Assert.assertEquals(count,0);
    }


    @Test(priority = 6)
    public void TestCountOfNodebasedOnRegEx()
    {
        Response response = getResponseBasedOnQueryParams("https://api.fixer.io/latest","GBP");
        JSONObject jsonObj = new JSONObject(response.getBody().asString());
        int count = getCountOfNodesMatchingRegEx(jsonObj,"[a-zA-Z]");
        System.out.println(count);
        Assert.assertEquals(count,1);
    }


    @Test(priority = 2,enabled = false)
    public void TestGetJsonResponseString()
    {
        //RestClient restClient = new RestClient();
        String[] response =  getJsonData( "GBP");
        for(String str : response)
        System.out.println(str);

    }

    @Test(priority = 2,enabled = false)
    public void TestResponseAsObject()
    {
        RestClient restClient = new RestClient();
        Response response = getResponseBasedOnQueryParams("https://api.fixer.io/latest","GBP");
        GBP gbp = response.getBody().as(GBP.class);
        System.out.println(gbp.toString());
        Assert.assertTrue(gbp.compareTo(gbp)==0,"Response are not the same");
    }





//    @Test(priority = 3)
//    public void TestNodeCountsInResponse()
//    {
//        RestClient restClient = new RestClient();
//        Response response = restClient.getResponseBasedOnQueryParams("https://api.fixer.io/latest","GBP");
//        String responseBody = response.getBody().asString();
//        String jsonExp = "$.rates";
//        JsonNode rates  = JsonPath.parse(responseBody).read(jsonExp, JsonNode.class);
//        System.out.println(rates);


//    }


}
