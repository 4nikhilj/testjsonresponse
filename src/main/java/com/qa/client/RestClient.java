package com.qa.client;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.qa.jsonClasses.GBP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.regex.Pattern;

public class RestClient {


    //Get method
    public static Response getResponseBasedOnQueryParams(String URI,String param)
    {
        RestAssured.baseURI = URI;
        RequestSpecification httpReq = RestAssured.given();
        httpReq.param("base",param);
        Response response = httpReq.get();
        return response;
    }

    public static String[] getJsonData(String param)
    {
        String data= null;
        data = getResponseBasedOnQueryParams("https://api.fixer.io/latest","GBP").getBody().asString();
        Gson gson = new Gson();
        GBP gbp = gson.fromJson(data,GBP.class);
        return new String[]{
                "Base"+ gbp.getBase(),
                "Date" + gbp.getDate(),
                "rates" + gbp.getRates()
        };
   }


   public static boolean checkForPresenseOfNode(String response,String node)
   {
       JSONObject obj = new JSONObject(response);
       JSONObject rates = obj.getJSONObject("rates");

       try {
           rates.getDouble(node);
           return true;

       } catch (JSONException e) {
           e.printStackTrace();
           return false;
       }
   }

    public static int getCountOfNode(JSONObject jsonObject)
    {
        Multimap<String ,Object> keyValPair =  ArrayListMultimap.create();
        keyValPair = parse(jsonObject,keyValPair);

        for(String k : keyValPair.keySet())
        {
            System.out.println(keyValPair.get(k));
        }
        return keyValPair.size();
   }


    public static int getCountOfNode(JSONObject jsonObject,String node)
    {
        Multimap<String ,Object> keyValPair =  ArrayListMultimap.create();
        keyValPair = parse(jsonObject,keyValPair);
        for(Object k : keyValPair.get(node))
        {
            System.out.println(k.toString());
        }
        return keyValPair.get(node).size();
    }

    public static int getCountOfNodesMatchingRegEx(JSONObject jsonObject,String regEx)
    {
        Multimap<String ,Object> keyValPair =  ArrayListMultimap.create();
        keyValPair = parse(jsonObject,keyValPair);
        ArrayList<String> matchingNodes= new ArrayList<String>();

        Pattern p = Pattern.compile(regEx);
        for(String key : keyValPair.keySet())
        {
            if(p.matcher(key).find())
            {
                matchingNodes.add(key);
            }
        }

        return matchingNodes.size();
    }

    public static Multimap<String,Object> parse(JSONObject json , Multimap<String,Object> out) throws JSONException{
        Iterator<String> keys = json.keys();
        while(keys.hasNext()){
            String key = keys.next();
            Object val = null;
            if ( json.get(key) instanceof JSONObject ) {
                JSONObject value = json.getJSONObject(key);
                parse(value,out);
            }
            else if(json.get(key) instanceof JSONArray)
            {
                for(Object obj : json.getJSONArray(key))
                {
                    parse((JSONObject)obj,out);
                }
            }
            else {
                val =json.get(key);
                if(val != null){
                    out.put(key,val);
                }
            }
        }
        return out;
    }
}
