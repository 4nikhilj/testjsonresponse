package com.qa.jsonClasses;

public class GBP implements Comparable<GBP> {
    private String base;
    private String date;
    private Rates rates;

    @Override
    public String toString() {
        return getBase() + ":" + getDate() + ":" + "\n" + getRates();
    }


    public int compareTo(GBP o) {
        if (this.getRates().compareTo(o.getRates()) != 0)
            return -1;
        else if (!this.getBase().equals(o.getBase()))
            return -1;
        else if (!this.getDate().equals(o.getDate()))
            return -1;
        else
            return 0;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }

}

class Rates implements Comparable<Rates> {
    public Double AUD;
    public Double BGN;
    public Double BRL;
    public Double CAD;
    public Double CHF;
    public Double CNY;
    public Double CZK;
    public Double DKK;
    public Double EUR;
    public Double HKD;
    public Double HRK;
    public Double HUF;
    public Double IDR;
    public Double ILS;
    public Double INR;
    public Double ISK;
    public Double JPY;
    public Double KRW;
    public Double MXN;
    public Double MYR;
    public Double NOK;
    public Double NZD;
    public Double PHP;
    public Double PLN;
    public Double RON;
    public Double RUB;
    public Double SEK;
    public Double SGD;
    public Double THB;
    public Double TRY;
    public Double USD;
    public Double ZAR;

    @Override
    public String toString() {
        return "\nKRW =" + KRW + "\n" +
                " MXN =" + MXN + "\n" +
                " USD =" + USD;
    }

    public int compareTo(Rates o) {

        if (this.equals(o)) {
            return 0;
        } else {
            return -1;
        }
    }
}
